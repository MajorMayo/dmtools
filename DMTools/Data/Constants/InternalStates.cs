﻿namespace Data.Constant
{
    public enum InternalStates
    {
        New = 0,
        UnModified = 1,
        Modified = 2,
        Deleted = 3
    }
}
