﻿using System;

namespace Data.VirtualObject
{
    public class DisplaySpecialAbility
    {
        public Guid Id { get; set; }
        public String DisplayName { get; set; }
    }
}
