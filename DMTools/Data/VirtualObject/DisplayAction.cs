﻿using System;

namespace Data.VirtualObject
{
    public class DisplayAction
    {
        public Guid Id { get; set; }
        public String DisplayName { get; set; }
    }
}
