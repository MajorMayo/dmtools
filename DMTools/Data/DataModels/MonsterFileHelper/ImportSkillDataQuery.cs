﻿using Data.Objects;
using System.Collections.Generic;

namespace Data.DataModels.MonsterFileHelper
{
    public class ImportSkillDataQuery
    {
        public List<Skill> Skills { get; set; }
    }
}
