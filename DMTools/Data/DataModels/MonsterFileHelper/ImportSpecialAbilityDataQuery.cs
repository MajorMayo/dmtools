﻿using Data.Objects;
using System.Collections.Generic;

namespace Data.DataModels.MonsterFileHelper
{
    public class ImportSpecialAbilityDataQuery
    {
        public List<SpecialAbility> SpecialAbilities { get; set; }
    }
}
