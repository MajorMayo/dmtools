﻿using Data.Objects;
using System.Collections.Generic;

namespace Data.DataModels.MonsterFileHelper
{
    public class ImportActionDataQuery
    {
        public List<Action> Actions { get; set; }
    }
}
