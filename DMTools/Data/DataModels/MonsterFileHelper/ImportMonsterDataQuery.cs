﻿using Data.Objects;
using System.Collections.Generic;

namespace Data.DataModels.MonsterFileHelper
{
    public class ImportMonsterDataQuery
    {
        public List<Monster> Monsters { get; set; }
    }
}
