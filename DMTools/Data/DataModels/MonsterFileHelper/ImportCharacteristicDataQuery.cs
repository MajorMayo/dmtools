﻿using Data.Objects;
using System.Collections.Generic;

namespace Data.DataModels.MonsterFileHelper
{
    public class ImportCharacteristicDataQuery
    {
        public List<Characteristic> Characteristics { get; set; }
    }
}
