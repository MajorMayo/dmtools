﻿using Data.VirtualObject;
using System.Collections.Generic;

namespace Data.DataModels.SkillWindow
{
    public class SaveSkillDataQuery
    {
        public List<DisplaySkill> DisplaySkills { get; set; }
    }
}
